package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataXmlSaveFasterXmlRequest;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        getDomainEndpoint().XmlSaveFasterXml(new DataXmlSaveFasterXmlRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-save-xml-fasterxml";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Save data to xml file with fasterxml";
    }

}
