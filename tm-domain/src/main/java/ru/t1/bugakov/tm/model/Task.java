package ru.t1.bugakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Status;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConstants.TABLE_TASK)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractUserOwnedModel {

    private static final long serialVersionUID = 1;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "";

    @NotNull
    @Column(nullable = false, name = "description")
    private String description = "";

    @NotNull
    @Column(nullable = false, name = "status")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

}
