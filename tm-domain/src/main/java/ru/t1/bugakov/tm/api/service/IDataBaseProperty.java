package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDataBaseProperty {

    @NotNull
    String getDBUsername();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUrl();

    @NotNull String getDBDriver();

    @NotNull String getDBDialect();

    @NotNull String getDBHbm2ddlAuto();

    @NotNull String getDBShowSql();

    @NotNull String getDBFormatSql();

    @NotNull String getDBSecondLvlCache();

    @NotNull String getDBFactoryClass();

    @NotNull String getDBUseQueryCache();

    @NotNull String getDBUseMinPuts();

    @NotNull String getDBRegionPrefix();

    @NotNull String getDBConfigFilePath();
}
