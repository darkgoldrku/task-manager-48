package ru.t1.bugakov.tm.api.service.dto;

import ru.t1.bugakov.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.bugakov.tm.dto.model.AbstractModelDTO;

public interface IAbstractDTOService<M extends AbstractModelDTO> extends IAbstractDTORepository<M> {

}
