package ru.t1.bugakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.model.ISessionRepository;
import ru.t1.bugakov.tm.model.Session;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable List<Session> findAll(@NotNull String userId) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();

    }

    @Override
    public @Nullable List<Session> findAllWithSort(@NotNull String userId, @Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Session> criteriaQuery = criteriaBuilder.createQuery(Session.class);
        @NotNull final Root<Session> root = criteriaQuery.from(Session.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public Session findById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);

    }

    @Override
    public int getSize(@NotNull String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Session m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();

    }

    @Override
    public @Nullable List<Session> findAll() {
        @NotNull final String jpql = "SELECT m FROM Session m";
        return entityManager.createQuery(jpql, Session.class).getResultList();

    }

    @Override
    public @Nullable List<Session> findAllWithSort(@Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Session> criteriaQuery = criteriaBuilder.createQuery(Session.class);
        @NotNull final Root<Session> from = criteriaQuery.from(Session.class);
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public @Nullable Session findById(@NotNull String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

}

