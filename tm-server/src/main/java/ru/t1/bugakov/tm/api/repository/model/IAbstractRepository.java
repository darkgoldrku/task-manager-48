package ru.t1.bugakov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.AbstractModel;

import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    void add(@NotNull final M model);

    List<M> findAll();

    List<M> findAllWithSort(@Nullable final String sortField);

    int getSize();

    M findById(@NotNull final String id);

    boolean existsById(@NotNull final String id);

    void update(M model);

    void clear();

    void remove(@NotNull final M model);

    void removeById(@NotNull final String id);

}
