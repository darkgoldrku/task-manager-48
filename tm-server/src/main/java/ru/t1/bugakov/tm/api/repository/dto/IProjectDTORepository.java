package ru.t1.bugakov.tm.api.repository.dto;

import ru.t1.bugakov.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IAbstractUserOwnedDTORepository<ProjectDTO> {

}
