package ru.t1.bugakov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IAbstractUserOwnedRepository<Task> {

    @Nullable List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

}
