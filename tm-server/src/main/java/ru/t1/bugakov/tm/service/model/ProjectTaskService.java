package ru.t1.bugakov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.model.ITaskRepository;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.model.IProjectService;
import ru.t1.bugakov.tm.api.service.model.IProjectTaskService;
import ru.t1.bugakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.bugakov.tm.exception.entity.TaskNotFoundException;
import ru.t1.bugakov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.bugakov.tm.exception.field.TaskIdEmptyException;
import ru.t1.bugakov.tm.exception.field.UserIdEmptyException;
import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectTaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    public ProjectTaskService(@NotNull IConnectionService connectionService, @NotNull IProjectService projectService) {
        super(connectionService);
        this.projectService = projectService;
    }

    @NotNull
    protected ITaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @Override
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProject(projectService.findById(projectId));
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProject(null);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            @Nullable final List<Task> tasks = repository.findAllByProjectId(userId, projectId);
            if (tasks == null) projectService.removeById(userId, projectId);
            else {
                for (@NotNull final Task task : tasks) repository.remove(userId, task);
                projectService.removeById(userId, projectId);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
