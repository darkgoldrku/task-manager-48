package ru.t1.bugakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.*;
import ru.t1.bugakov.tm.api.service.*;
import ru.t1.bugakov.tm.api.service.dto.*;
import ru.t1.bugakov.tm.endpoint.*;
import ru.t1.bugakov.tm.service.*;
import ru.t1.bugakov.tm.service.dto.*;
import ru.t1.bugakov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @Getter
    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @Getter
    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @Getter
    @NotNull
    private final IUserDTOService userService = new UserDTOService(connectionService, projectService, taskService, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @Getter
    @NotNull
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(connectionService, projectService);

    {
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = this.getPropertyService().getServerHost();
        @NotNull final String port = this.getPropertyService().getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }


    private void initDemoData() throws SQLException {
        /*userService.create("admin", "admin", Role.ADMIN);
        @NotNull final User admin = userService.findByLogin("admin");
        projectService.create(admin.getId(), "p111", "222");
        taskService.create(admin.getId(), "t001", "222");
        taskService.create(admin.getId(), "t002", "444");

        userService.create("test", "test", "test@tast.ru");
        @NotNull final User test = userService.findByLogin("test");
        projectService.create(test.getId(), "p333", "444");
        taskService.create(test.getId(), "t003", "222");
        taskService.create(test.getId(), "t004", "444");

        userService.create("user", "user", "user@user.ru");*/
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start() throws SQLException {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        loggerService.info("TASK MANAGER IS SHUTTING DOWN");
    }

}